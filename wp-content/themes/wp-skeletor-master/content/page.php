<?php
/**
 * The page template
 *
 * @package WordPress
 * @subpackage SKEL-ETOR
 * @since SKEL-ETOR 1.0
 */
?>

<article id="post-<?php echo get_the_ID(); ?>" <?php post_class() ?>>
	<header>
		<h1><?php the_title(); ?></h1>
	</header>
	<div class="entry">
	<h1><?php the_field('text'); ?></h1>
	<div class="acf-fields" ><?php _print_acffields(get_the_ID);?></div>
		<?php the_content(); ?>
		
	</div>
	<footer>
		<?php do_action('skel_etor_page_footer'); ?>
	</footer>
</article>
