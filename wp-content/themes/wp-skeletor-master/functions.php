<?php
/**
 * SKEL-ETOR functions and definitions
 *
 * @package WordPress
 * @subpackage SKEL-ETOR
 * @since SKEL-ETOR 1.0
 *
 * match case - find -> replace: 'SKEL-ETOR' -> Template Names ("SKEL-ETOR 2012") E.g. The Nest
 * match case - find -> replace: 'SKEL_ETOR' -> Theme/Front-End names ("SKEL_ETOR_URL") E.g. THE_NEST_CONSTANT
 * match case - find -> replace: 'skel_etor' -> Function/Variable names ("skel_etor"_excerpt();) E.g. the_nest
 * match case - find -> replace: 'skel-etor' -> Option Names (some_function("skel-etor"-option)); E.g. the-nest
 *
 */

require( get_template_directory() . '/theme/constants.php');
require( get_template_directory() . '/theme/debug.php');
require( get_template_directory() . '/theme/classes.php');
require( get_template_directory() . '/theme/functions.php');
require( get_template_directory() . '/theme/enqueue.php');
require( get_template_directory() . '/theme/shortcodes.php');
require( get_template_directory() . '/theme/setup.php');
require( get_template_directory() . '/theme/actions.php');
require( get_template_directory() . '/theme/filters.php');
require( get_template_directory() . '/theme/theme-actions.php');
require( get_template_directory() . '/theme/theme-hooks.php');
/* ACF Fields **/
add_action( 'wp_enqueue_scripts', 'googleMapScripts' );
function _print_acffields(){
	if($fields = get_fields($post_id)){
		foreach( $fields as $field_name => $value ){
			$field = get_field_object($field_name, false, array('load_value' => false));
			 printThisField($field, $post_id);
				
		}
	}
}
function printThisField($field,$post_id){
	$fieldData=get_field($field['name'],$post_id);
	if($fieldData){
		switch($field['type']){
			case 'text':
				echo  '<p><label>'.$field['label'].'</label></p><p>'.$fieldData.'</p>';
				break;
			case 'date_picker':
				$date = DateTime::createFromFormat('Ymd', $fieldData);
				if($date){
					echo '<p><label>'.$field['label'].'</label></p><p>'.$date->format('d-m-Y').'</p>';
				}else{
					echo '<p><label>'.$field['label'].'</label></p><p>'.$fieldData.'</p>';
				}
				break;
			case 'color_picker':
				echo '<p><label>'.$field['label'].'</label></p><p style="background-color:'.$fieldData.';height:10px;"></p>';
				break;
			case 'image':
				
				echo '<p><label>'.$field['label'].'</label></p><p><img src="'.$fieldData['url'].'" height="'.$fieldData['height'].'" width="'.$fieldData['width'].'" title="" /></p>';
				break;	
			case 'wysiwyg':
				echo '<p><label>'.$field['label'].'</label></p><div class="editor-content">'.$fieldData.'</div>';
				break;	
			case 'file':
				echo '<p><label>'.$field['label'].'</label></p><div class="editor-content"><a href="'.$fieldData['url'].'">'.$fieldData['title'].'</a></div>';
				break;	
			case 'google_map':

				echo '<div class="acf-maps" style="width: 100%;	height: 400px;	border: #ccc solid 1px;	margin: 20px 0;">
						<div class="marker" data-lat="'.$fieldData['lat'].'" data-lng="'.$fieldData['lng'].'"></div>
					</div>';
				
				break;
			case 'repeater':
				if($fieldData){
					$lastBigRow=count($fieldData)%2!=0?true:false;
					
					$idx=1;
					foreach($fieldData as $Singlefield)
					{
						
						$colMd=ceil(12/count($Singlefield));
						
						foreach($Singlefield as $key=>$cField){
							$strOutput.='<div class="col-md-'.$colMd.'">';
							if(!is_array($cField)){
								$strOutput.=$cField;
							}
							$strOutput.='</div>';
						}
						$idx++;
						
					}
					echo $strOutput;
				}
				break;
		}
	}
}
function googleMapScripts() {
	
	wp_enqueue_script( 'goog-map-library', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array(), '1.0.0', true );
	wp_enqueue_script( 'goolge-map-implementation',  get_template_directory_uri().'/js/googlemap.js', array(), '1.0.0', true );
}
/** END **/
