<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'genesis');

/** MySQL database username */
define('DB_USER', 'impinge');

/** MySQL database password */
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5>-h/XOV^I%)oWGQ+SvdbqjrNC*dPl02/68bGT1k+3lOtP6O+A4rnZVu6noY,h|.');
define('SECURE_AUTH_KEY',  '~TNP2 nBv?=3z.*uyL~ 3wBpqO]5Rs3.ZMEc&X/:C1(KW6}J 1SaL*e0<otF aVz');
define('LOGGED_IN_KEY',    'y]p{1u.HF+Fx{`/^YX,57<Rp9ybjNjk@Xmz&^)ToRJaDPcS:@0Jak:pDw}=CcF9K');
define('NONCE_KEY',        'imn.XY5,7Ochc!M;SnqG{J(t4k0jS-Dj4Qj!Q9Uel4/Mg[?.qH*>@HF+s+r?(zFE');
define('AUTH_SALT',        '|#QDS!{,stP$@>I&#RGt]-kc5m1;HPag 6{D_fW<5qG< xKzRfY(o/=O3!7B]PI%');
define('SECURE_AUTH_SALT', '#n+a[s&r{V1rJ6FlN|[|M2w%z)OxK,hH(1jekAvH7stT5`_9U$;<(>L6E<??0Qyz');
define('LOGGED_IN_SALT',   'lp9ctnz^1[uTr,,V=y~IsA^},MHQj6/O|%A]Ds--+Z+syUFpHGc${.B-%8GeLKXq');
define('NONCE_SALT',       '9qMdcSE{BU~9.DKoqhO.Y>u}tcAg{E!r: 6WFZ*FvlB^$JZ(: ^4cp+z-x*PM|PC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
